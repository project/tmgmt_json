<?php

/**
 * @file
 */

namespace Drupal\tmgmt_json\Plugin\tmgmt_file\Format;

use Drupal\Core\Annotation\Translation;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\Annotation\FormatPlugin;
use Drupal\tmgmt_json\Plugin\tmgmt_file\TmgmtJsonFormatPluginBase;


/**
 * Class Json
 *
 * @package Drupal\tmgmt_json\Plugin\tmgmt_file\Format
 *
 * Export into JSON format
 *
 * @FormatPlugin(
 *   id="json",
 *   label=@Translation("JSON")
 * )
 */
class Json extends TmgmtJsonFormatPluginBase {
  /**
   * {@inheritdoc}
   */
  public function export(JobInterface $job, $conditions = []) {
    $this->setFormat($this->getClassName());
    return parent::export($job, $conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function import($imported_file, $is_file = TRUE) {
    $this->setFormat($this->getClassName());
    return parent::import($imported_file, $is_file);
  }

  /**
   * {@inheritdoc}
   */
  public function validateImport($imported_file, $is_file = TRUE) {
    $this->setFormat($this->getClassName());
    return parent::validateImport($imported_file, $is_file);
  }
}
