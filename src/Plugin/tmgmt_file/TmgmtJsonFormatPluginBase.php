<?php

/**
 * @file
 */

namespace Drupal\tmgmt_json\Plugin\tmgmt_file;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\Format\FormatInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TmgmtJSONFormatPluginBase
 *
 * @package Drupal\tmgmt_json\Plugin
 */
abstract class TmgmtJSONFormatPluginBase implements FormatInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * @var string $format
   */
  protected $format;

  /**
   * @var string $json
   */
  protected $json;

  /**
   * @var \Drupal\Core\File\FileSystemInterface $file_system;
   */
  protected $file_system;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  protected $messenger;

  /**
   * @var \Drupal\tmgmt\Data $tmgmt_data
   */
  protected $tmgmt_data;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, FileSystemInterface $file_system, MessengerInterface $messenger, Data $tmgmt_data) {
    $this->file_system = $file_system;
    $this->messenger = $messenger;
    $this->tmgmt_data = $tmgmt_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('messenger'),
      $container->get('tmgmt.data')
    );
  }

  /**
   * @param string $format
   */
  protected function setFormat($format) {
    $this->format = strtolower($format);
  }

  /**
   * Get format string.
   *
   * @return string
   */
  protected function getFormat() {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function export(JobInterface $job, $conditions = []) {
    $this->json = [
      'job_id' => $job->id(),
      'source' => $job->getRemoteSourceLanguage(),
      'target' => $job->getRemoteTargetLanguage(),
      'translations' => []
    ];

    foreach ($job->getItems($conditions) as $item) {
      $data = $this->tmgmt_data->filterTranslatable($item->getData());

      foreach ($data as $key => $value) {
        $this->json['translations'][] = [
          'key' => $item->id() . '][' . $key,
          'parent label' => (isset($value['#parent_label'][0]) ? $value['#parent_label'][0] : ''),
          'label' => (isset($value['#label']) ? $value['#label'] : ''),
          'original' => (isset($value['#text']) ? $value['#text'] : ''),
          'translated' => (isset($value['#text']) ? $value['#text'] : '')
        ];
      }
    }

    return json_encode($this->json);
  }

  /**
   * {@inheritdoc}
   */
  public function import($imported_file, $is_file = TRUE) {
    $result = [];

    foreach ($this->json->translations as $row) {
      $result[$row->key]['#text'] = $row->translated;
    }

    return $this->tmgmt_data->unflatten($result);
  }

  /**
   * {@inheritdoc}
   */
  public function validateImport($imported_file, $is_file = TRUE) {
    $real_path = $this->file_system->realpath($imported_file);
    $file_type = strtolower(pathinfo($real_path, PATHINFO_EXTENSION));

    if ($file_type != $this->getFormat()) {
      $this->messenger->addError($this->t('Missing file format, please choose another plugin.'));
      return FALSE;
    }
    $this->json = json_decode(file_get_contents($real_path));

    // check tmgmt job
    if (!$job = Job::load($this->json->job_id)) {
      $this->messenger->addError($this->t('Internal TMGMT error, unable load job.'));
      return FALSE;
    }
    // check languages
    $source = $this->json->source;
    $target = $this->json->target;
    if ($source != $job->getRemoteSourceLanguage() || $target != $job->getRemoteTargetLanguage()) {
      $this->messenger->addError($this->t('Internal TMGMT error, missing languages.'));
      return FALSE;
    }
    return $job;
  }

  /**
   * Get short class name.
   */
  public function getClassName() {
    return (new \ReflectionClass($this))->getShortName();
  }
}
